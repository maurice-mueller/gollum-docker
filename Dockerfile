FROM ruby

# PREREQUISTES
RUN apt-get -y update && apt-get -y install libicu-dev cmake && rm -rf /var/lib/apt/lists/*

# GOLLUM
RUN gem install github-linguist
RUN gem install gollum
RUN gem install org-ruby  # optional

# PLANTUML
RUN apt-get -y update && apt-get -y install maven graphviz openjdk-11-jdk git-core
RUN git clone https://github.com/plantuml/plantuml-server.git
RUN cd /plantuml-server && mvn package
RUN cd /plantuml-server && mvn jetty:run &

# SET UP GIT
RUN git config --global user.email "nonexisiting@example.com"
RUN git config --global user.name "Gollum Wiki Process"

# WIKI DIR
RUN mkdir /wiki
RUN git init /wiki
RUN cd /wiki && git checkout -b main
RUN mkdir /wiki/pages

# ENTRYPOINT
ENTRYPOINT ["gollum", "/wiki", "--port", "8088", "--emoji", "--ref", "main", "--base-path", "/", "--page-file-dir", "pages"]
EXPOSE 8088
